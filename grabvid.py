#!/usr/bin/env python3
from __future__ import unicode_literals
import youtube_dl
import xmltodict
from urllib import request
from os.path import getsize

FEED_URL='https://www.youtube.com/feeds/videos.xml?channel_id=UCVqlDOUyIjMWqBUhp73a90g'
VIDEO_URL_PREFIX = 'https://www.youtube.com/watch?v='

class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')

ydl_opts = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'vorbis',
        'preferredquality': '192',
    }],
    'logger': MyLogger(),
    'progress_hooks': [my_hook],
}


def get_video_uri(video_id):
    return VIDEO_URL_PREFIX + video_id

def get_videos_list():
    vid_list = []
    with request.urlopen(FEED_URL) as response:
        xml = response.read().decode()
    feed_dict = xmltodict.parse(xml)
    for video in feed_dict['feed']['entry']:
        vid_list.append({
            'videoid': video['yt:videoId'],
            'title': video['title'],
            'description': video['media:group']['media:description'],
            'date': video['published'],
            'link': get_video_uri(video['yt:videoId'])
        })
    return vid_list

def choose_video():
    videos = get_videos_list()
    for index, video in enumerate(videos):
        print(f'[{index}] {video["title"]}')
    choice = -1
    while choice < 0 or choice >= len(videos) or type(choice) != int:
        try:
            choice = int(input('\n\nChoose a video\n?> '))
        except ValueError:
            choice = -1
    video = videos[choice]
    return video

def download_as_audio(video, destination):
    filename = f'{video["title"].replace(" ","_").replace("|","")}.%(ext)s'
    outpath = f'{destination}/{filename}'
    print(f'Downloading new item to {outpath}')
    opts = ydl_opts.copy()
    opts['outtmpl'] = outpath
    with youtube_dl.YoutubeDL(opts) as ydl:
        ydl.download([video['link']])
    video['path'] = outpath[:-7] + 'ogg'
    video['filename'] = filename[:-7] + 'ogg'
    video['bytesize'] = getsize(outpath[:-7] + 'ogg')
    return video
