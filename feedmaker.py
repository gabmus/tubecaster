#!/usr/bin/env python3
import datetime
import pytz
import subprocess

ITEMS_BEGIN_MARKER='<!--TUBECASTER_ITEMS_BEGIN_MARKER-->'
ITEMS_END_MARKER='<!--TUBECASTER_ITEMS_END_MARKER-->'

RSS_PREFIX = f'''
<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" encoding="UTF-8" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:admin="http://webns.net/mvcb/" xmlns:atom="http://www.w3.org/2005/Atom/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:googleplay="http://www.google.com/schemas/play-podcasts/1.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:fireside="http://fireside.fm/modules/rss/fireside">
    <channel>
        <generator>gitlab dot com slash gabmus slash tubecaster</generator>
        <title>Tech Pills Live</title>
        <link>https://techpills.technology</link>
        <pubDate>{datetime.datetime.now(pytz.timezone("Europe/Rome")):%a, %d %b %Y %H:%M:%S %z}</pubDate>
        <description>Audio-only version of the livestreams from the Tech Pills YouTube channel https://youtube.com/TechPillsNet</description>
        <language>en-us</language>
        <itunes:type>episodic</itunes:type>
        <itunes:subtitle>Audio-only version of the livestreams from the Tech Pills YouTube channel</itunes:subtitle>
        <itunes:author>Gabriele Musco aka GabMus aka Tech Pills</itunes:author>
        <itunes:summary>Audio-only version of the livestreams from the Tech Pills YouTube channel https://youtube.com/TechPillsNet</itunes:summary>
        <itunes:image href="https://techpills.technology/img/podcast_coverart.png"/>
        <itunes:explicit>yes</itunes:explicit>
        <itunes:owner>
            <itunes:name>Gabriele Musco</itunes:name>
            <itunes:email>emaildigabry@gmail.com</itunes:email>
        </itunes:owner>
        <itunes:category text="Technology">
            <itunes:category text="Tech News"/>
        </itunes:category>
        {ITEMS_BEGIN_MARKER}
'''

RSS_SUFFIX = f'''
        {ITEMS_END_MARKER}
    </channel>
</rss>
'''

FILE_PREFIX='https://techpills.technology/podcast/'

NLINE = '\n'

def make_item(video):
    return f'''
        <item>
            <title>{video["title"]}</title>
            <link>{video["link"]}</link>
            <!-- <guid isPermaLink="false">ab65af13-b3f9-4d4d-a827-5853b765bd54</guid> -->
            <pubDate>{datetime.datetime.fromisoformat(video["date"]):%a, %d %b %Y %H:%M:%S %z}</pubDate>
            <author>Gabriele Musco</author>
            <enclosure url="{FILE_PREFIX}/{video['filename']}" length="{video['bytesize']}" type="audio/ogg"/>
            <itunes:episodeType>full</itunes:episodeType>
            <itunes:author>Gabriele Musco</itunes:author>
            <itunes:subtitle>Tech Pills Live</itunes:subtitle>
            <itunes:duration>{subprocess.check_output(['soxi', '-d', video['path']]).decode().strip().split('.')[0]}</itunes:duration>
            <itunes:explicit>no</itunes:explicit>
            <itunes:image href="https://techpills.technology/img/podcast_coverart.png"/>
            <description>{video['description']}</description>
            <!-- <itunes:keywords></itunes:keywords> -->
            <content:encoded>
                <![CDATA[<p>{video['description'].replace(NLINE, '<br />'+NLINE)}</p>]]>
            </content:encoded>
            <itunes:summary>
                <![CDATA[<p>{video['description'].replace(NLINE, '<br />'+NLINE)}</p>]]>
            </itunes:summary>
        </item>
    '''

def get_items(feed_str):
    return feed_str[
        feed_str.find(ITEMS_BEGIN_MARKER)+len(ITEMS_BEGIN_MARKER):
        feed_str.find(ITEMS_END_MARKER)
    ]
