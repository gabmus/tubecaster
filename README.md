# tubecaster

Command line application, for internal use, to convert some select videos from the Tech Pills YouTube channel to ogg, and make the whole thing into a usable podcast-style rss feed.

Licensed under AGPL3, feel free to extend/adapt this to your use, just make sure to share your changes!
