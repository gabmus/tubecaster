import argparse
import pathlib
import feedmaker
import grabvid

parser = argparse.ArgumentParser(description = 'Tubecaster: YT to podcast')
parser.add_argument(
    'rss_file',
    metavar='rss_file',
    type=str,
    nargs=1,
    help='The path to the existing rss feed file'
)
parser.add_argument(
    'new_item_path',
    metavar='new_item_path',
    type=str,
    nargs=1,
    help='The path where to save the new item'
)


def main():
    args = parser.parse_args()
    rss_path = pathlib.Path(args.rss_file[0])
    if not rss_path.exists():
        print('Error: provided rss_file path does not exist')
        exit(1)
    elif rss_path.is_dir():
        print('Error: provided rss_file path is a directory')
        exit(1)
    n_item_path = pathlib.Path(args.new_item_path[0])
    if not n_item_path.exists():
        print('Error: provided new_item_path does not exist')
    if not n_item_path.is_dir():
        print('Error: provided new_item_path must be a directory')
    rss = None
    with open(rss_path) as fd:
        rss = fd.read()
        fd.close()
    if not rss:
        print('Error: provided rss_file is empty')
        exit(1)
    items = feedmaker.get_items(rss)
    n_video = grabvid.download_as_audio(grabvid.choose_video(), n_item_path)
    items = feedmaker.make_item(n_video) +'\n'+ items
    n_rss = feedmaker.RSS_PREFIX + items + feedmaker.RSS_SUFFIX
    with open(rss_path, 'w') as fd:
        fd.write(n_rss)
        fd.close()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        exit(0)
